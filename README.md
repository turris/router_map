Map of Turris router components
===============================
Software in Turris routers called Turris OS is distributed in form of packages.
Those can be grouped to components/features of router and all that is then
installable in form of lists. This project documents middle layer of these
dependencies, meaning it documents components in respect to them being part of
specific list and provided by some package. Primary reason of this documentation
is to have list of expected components supported on routers and their potential
dependencies.

Current master map rendered can be found
[here](https://turris.pages.labs.nic.cz/router_map/map.svg).

Legend
------
Components are grouped together according to their `list` field. Color of their
boxes depends on `func` field as follows:

* __red__: `hardware`
* __orange__: `system`
* __green__: `primary`
* __cyan__: `secondary`
* __blue__: `config`
* __purple__: `extension`

Edges are oriented and points towards `dependency`. Dashed edges are optional
dependencies. All dependencies project some kind of requirement. In general if
there is failure in pointed component then also component pointing to it can be
considered as being in failure.

Generating components map
-------------------------
Map can be generated simply by running `generate.py`.

This script requires Python3 and following packages:
* PyYAML
* PyGraphviz

To install it you can call:
```bash
pip3 install -r requirements.txt
```
or (if you already used python3 virtualenv):
```bash
pip install -r requirements.txt
```

File format
-----------
Files are in YAML file format. All files should have file extension `.yml`.

Following options are expected:
* `depends`: This defines dependency on other components.
* `opt_depends`: This defines optional dependency on other component. Meaning that
  such component can work without it but with given optional component it can do
  more.
* `doc`: required documentation of component.
* `url`: optional URL to documentation of specific component.
* `func`: Specification of criticality level of given component. Levels are
  defined as follows (in importance order):
  * `hardware`: Component closely linked with hardware. Its failure causes almost
	unrecoverable state from user standpoint. Expert knowledge and special tools
	are required.
  * `system`: Component that is part of minimal software that is required for
	hardware enablement.
  * `primary`: Component that is part of primary router feature set. It provides
	feature that is highly expected from standard router.
  * `secondary`: Component that is extending feature set of router. It is not part
	of standard router feature set but is part of Turris router feature set.
  * `config`: Component that is intended for configuration of the router.
  * `extension`: Component that extends router's feature set but we can expect
	that its failure is will not immediately decremental to routers functionality
	for standard users.
* `list`: Which list is used for installation of this component.
* `packages`: List of packages that are primarily part of this component. This is
  just complementary information and might not be full list of required packages.
* `maintainer`: Person responsible for given component. Provide only email.
