#!/usr/bin/env python3
import os
import yaml
import pygraphviz as pgv

known_keys = (
    'depends', 'opt_depends', 'doc', 'url', 'func', 'list', 'packages',
    'maintainer'
    )
func_levels = {
    'hardware': 'red',
    'system': 'orange',
    'primary': 'green',
    'secondary': 'cyan',
    'config': 'blue',
    'extension': 'purple'
    }


comps = dict()

# Parse
for file in os.listdir(os.path.dirname(os.path.realpath(__file__))):
    if not file.endswith('.yml') or file.startswith('.'):
        continue
    data = None
    with open(file, 'r') as f:
        data = yaml.load(f)
    for comp in data:
        if comp in comps:
            print("Component defined multiple times: " + str(comp))
            exit(1)
        for key in data[comp]:
            if key not in known_keys:
                print("Unknown key of component {}: {}".format(str(comp),
                                                               str(key)))
                exit(1)
        if 'doc' not in data[comp]:
            print("Missing doc for component: {}".format(str(comp)))
            exit(1)
        if 'func' in data[comp]:
            if data[comp]['func'] not in func_levels:
                print("Unknown criticality level of component {}: {}".
                      format(str(comp), str(data[comp]['func'])))
                exit(1)
        comps[comp] = data[comp]


# Verify function ciriticality escalation
# TODO
# Detect redundant edges and report them as warnings
# TODO

G = pgv.AGraph(strict=False, directed=True, rankdir='LR')
Gsub = dict()
# Generate graph
for comp in comps:
    # Graph or subgraph
    graph = G
    if 'list' in comps[comp]:
        if comps[comp]['list'] not in Gsub:
            Gsub[comps[comp]['list']] = G.add_subgraph(
                name="cluster_" + comps[comp]['list'],
                label=comps[comp]['list'],
                style='filled', color='lightgrey', fontname="times-bold"
                )
        graph = Gsub[comps[comp]['list']]
    # Add node
    label = "~{}~\n{}".format(comp, comps[comp]['doc'])
    if 'url' in comps[comp]:
        label = label + "\n{}".format(comps[comp]['url'])
    if 'packages' in comps[comp]:
        label = label + "\n: {} :".format(', '.join(comps[comp]['packages']))
    if 'maintainer' in comps[comp]:
        label = label + "\n<{}>".format(comps[comp]['maintainer'])
    color = 'black'
    if 'func' in comps[comp]:
        color = func_levels[comps[comp]['func']]
    graph.add_node(comp, shape='box', label=label, color=color)
    # Create edges
    if 'depends' in comps[comp]:
        for c in comps[comp]['depends']:
            if c not in comps:
                print("Component {} depends on unknown component: {}".
                      format(comp, c))
            else:
                G.add_edge(comp, c)
    if 'opt_depends' in comps[comp]:
        for c in comps[comp]['opt_depends']:
            if c not in comps:
                print("Component {} optionally depends on unknown component: {}".
                      format(comp, c))
            else:
                G.add_edge(comp, c, style='dashed')

G.layout(prog='dot')
G.draw('map.png')
G.draw('map.svg')
